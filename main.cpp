#include <chrono>
#include <fstream>
#include <iostream>
#include <string>
#include <list>
#include <vector>

class Component
{
public:
	virtual ~Component() = default;
};

class HealthComponent : public Component
{
private:
	int m_health = 0;
};

class AttackComponent : public Component
{
private:
	unsigned int m_attackPower = 0;
};

class MovementComponent : public Component
{
private:
	float m_speed = 0;
};


class Rectangle2D
{
	float rectX1;
	float rectY1;
	float rectX2;
	float rectY2;

public:
	Rectangle2D() {
		rectX1 = 0;
		rectY1 = 0;
		rectX2 = 0;
		rectY2 = 0;
	}

	Rectangle2D(float x1, float y1, float width, float height) {
		rectX1 = x1;
		rectY1 = y1;
		rectX2 = x1 + width;
		rectY2 = y1 + height;
	}

	float getX1() {
		return rectX1;
	}

	float getY1() {
		return rectY1;
	}

	float getX2() {
		return rectX2;
	}

	float getY2() {
		return rectY2;
	}
};

class Entity
{
private:
	Rectangle2D entityBoundingBox = Rectangle2D();
	std::list<Component> listOfComponents;

public:
	Entity() {

	}

	void addComponent(Component newComponent) {
		listOfComponents.push_front(newComponent);
	}

	void setBoundingBox(Rectangle2D boundingBox) {
		entityBoundingBox = boundingBox;
	}

	Rectangle2D getBoundingBox() {
		return entityBoundingBox;
	}

protected: 
	bool deepcopy(const Entity& rhs) {
		bool deepCopySuccess = false;
		if (&rhs != this) {
			this->entityBoundingBox = rhs.entityBoundingBox;
			this->listOfComponents = rhs.listOfComponents;
			deepCopySuccess = true;
		}
		return deepCopySuccess;
	}
};

bool entityOverlapCheck(Entity firstEntity, Entity secondEntity)
{
	if (firstEntity.getBoundingBox().getY1() > secondEntity.getBoundingBox().getX1() ||
		firstEntity.getBoundingBox().getX1() < secondEntity.getBoundingBox().getY1() ||
		firstEntity.getBoundingBox().getY2() > secondEntity.getBoundingBox().getX2() ||
		firstEntity.getBoundingBox().getX2() > secondEntity.getBoundingBox().getY2()) {
		return true;
	}
	else {
		return false;
	}
}

int main(int argc, const char* argv[])
{
	if (argc < 2)
	{
		std::cerr << "Specify a file to run this program.\n";
		return 1;
	}
	std::cout << "Running program against file: " << argv[1] << "\n";
	std::ifstream file(argv[1], std::ios::binary);
	if (!file.is_open())
	{
		std::cerr << "Failed to open file.\n";
		return 2;
	}
	unsigned int numberOfEntities = 0;
	if (!(file >> numberOfEntities))
	{
		std::cerr << "Failed to get number of Entities from file.\n";
		return 3;
	}

	std::vector<Entity> listOfEntities;

	for (unsigned int i = 0; i < numberOfEntities; ++i)
	{
		Entity newEntity = Entity();

		float x = 0;
		float y = 0;
		float width = 0;
		float height = 0;

		if (!(file >> x >> y >> width >> height))
		{
			std::cerr << "Error getting bounds on line " << i + 1 << ".\n";
			return 4;
		}
		// Not every Entity has Components.
		std::string componentTypes;
		const auto currentPos = file.tellg();
		file >> componentTypes;

		if (!componentTypes.empty() && !std::isalpha(componentTypes.back()))
		{
			file.seekg(currentPos);
			componentTypes.clear();

		}
		for (const char type : componentTypes)
		{
			switch (type)
			{

			case 'H':
				newEntity.addComponent(HealthComponent());
				break;
			case 'A':
				newEntity.addComponent(AttackComponent());
				break;
			case 'M':
				newEntity.addComponent(MovementComponent());
				break;
			default:
				std::cerr << "Unknown Component type: " << type << "\n";
				break;
			}
		}

		newEntity.setBoundingBox(Rectangle2D(x, y, width, height));
		listOfEntities.push_back(newEntity);
	}
	file.close();
	const auto start = std::chrono::high_resolution_clock::now();

	int numOfIntersections = 0;

	for (int i = 0; i < listOfEntities.size(); i++) {
		for (int y = i + 1; y < listOfEntities.size(); y++) {
			if (entityOverlapCheck(listOfEntities[i], listOfEntities[y]) ){
				numOfIntersections++;
			}
		}
	}

	std::cout << "Found Intersections: " << numOfIntersections << "\n";

	const auto end = std::chrono::high_resolution_clock::now();
	const auto runMS =
		std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	std::cout << "Algorithm executed in " << runMS.count() << "ms.\n";
	return 0;
}
